//
//  DefaultError.swift
//
//  Copyright (c) Andres F. Lozano
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//


import Foundation


open class DefaultError: HError {
  
  
  // ---------------------------------------------------------------------
  // MARK: Variables
  // ---------------------------------------------------------------------
  
  
  open var status: Int!
  open var error: String?
  
  
  required public init(status: Int) {
    self.status = status
  }
  
  open var errorDescription: String? {
    return error
  }
  
}

extension DefaultError {
  
  
  func errorFromSelf<E:HError>() throws -> E {
    guard
      let data = try? JSONEncoder().encode(self),
      let aux = try? JSONDecoder().decode(E.self, from: data)
    else {
      throw ErrorDefaultError.cannotBeConverted
    }
    return aux
  }
  
  
  // ---------------------------------------------------------------------
  // MARK: Helpers func
  // ---------------------------------------------------------------------
  
  
  public class func createError(code: Int? = nil) -> DefaultError {
    
    let customError = DefaultError.init(status: code ?? -1)
    customError.error = "Something went wrong, try again"
    return customError
  }
  
  
  public class func getErrorFrom<E: HError>(data: Data, code: Int) throws -> E {
    guard
      var item = try? JSONDecoder().decode(E.self, from: data)  else {
      throw ErrorDefaultError.cannotBeConvertedFromData
    }
    item.status = code
    return item
  }
}

public protocol HError: Codable, LocalizedError {
  var status: Int! { get set }
  
  init(status: Int) 
}


enum ErrorDefaultError: Error {
  case cannotBeConverted
  case cannotBeConvertedFromData
}
