//
//  ApiProvider.swift
//
//  Copyright (c) Andres F. Lozano
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

import Alamofire
import Foundation


public class ApiRequestManager {
  
  
  public typealias Completation<T, E: HError>  = (Result<T, E>) -> Void
  
  
  // ---------------------------------------------------------------------
  // MARK: variables
  // ---------------------------------------------------------------------
  
  
  public static var url: String = ""
  public static var handleGetToken: (() -> String?)? = nil
  public static var dateFormat: String = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
  public static var strategyDecoder: JSONDecoder?
  public static var isDebug: Bool = true
  private (set) var request: URLRequest?
  
  
  // ---------------------------------------------------------------------
  // MARK: Constructor
  // ---------------------------------------------------------------------
  

  public init() { }
  
  
  // ---------------------------------------------------------------------
  // MARK: Helpers func
  // ---------------------------------------------------------------------
  
  
  @discardableResult
  public func executeDecodable<T:Decodable, E:HError>(
    route:BaseRoute,
    decoder: JSONDecoder = JSONDecoder(),
    timeoutRequest: Double = 30,
    completion:@escaping Completation<T, E>
  ) -> DataRequest? {
    
    handleDataRequest(route: route, decoder: decoder, timeoutRequest: timeoutRequest)?
      .responseDecodable(decoder: Self.strategyDecoder ?? customDateFormatter()) { [weak self] (response: DataResponse<T, AFError>) in
        self?.handleSetup(response: response)
        switch response.result {
          case .success(let value): completion(.success(value))
          case .failure(let error):
            self?.handleError(code: error.responseCode, data: response.data, completion: completion)
        }
      }
  }
  
  
  @discardableResult
  public func execute<T, E:HError>(
    route:BaseRoute,
    decoder: JSONDecoder = JSONDecoder(),
    timeoutRequest: Double = 30,
    completion:@escaping Completation<T?, E>
  ) -> DataRequest? {

    handleDataRequest(route: route, decoder: decoder, timeoutRequest: timeoutRequest)?
      .responseJSON { [weak self] response in
        self?.handleSetup(response: response)
        switch response.result {
          case .success(let value):
            guard let value = value as? T else {
              return completion(.success(nil))
            }
            completion(.success(value))
          case .failure(let error):
            self?.handleError(code: error.responseCode, data: response.data, completion: completion)
        }
      }
  }
  
  
  private func handleDataRequest(
    route:BaseRoute,
    decoder: JSONDecoder = JSONDecoder(),
    timeoutRequest: Double = 30
  ) -> DataRequest? {
    guard
      let endpoint = route.getEndPoint(),
      let urlRequest = endpoint.asURLRequest(timeoutRequest: timeoutRequest)
    else { return nil }
    return urlRequest.validate()
  }
  
  
  private func handleSetup<T>(response: DataResponse<T, AFError>) {
    self.request = response.request
    if Self.isDebug { debugPrint(response) }
  }
  
  
  private func handleError<T, E:HError>(code: Int?, data: Data?, completion: Completation<T, E>) {
    guard let data = data,
          let err:E = try? DefaultError.getErrorFrom(data: data, code: code ?? -1)
    else {
      let defaultError = DefaultError.createError(code: code)
      if let aux:E = try? defaultError.errorFromSelf() {
        completion(.failure(aux))
      }
      return
    }
    completion( .failure(err))
  }
  
  
  private func customDateFormatter() -> JSONDecoder  {
    let decoder = JSONDecoder()
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = Self.dateFormat
    decoder.dateDecodingStrategy = .formatted(dateFormatter)
    return decoder
  }
}


public protocol BaseRoute {
  func getEndPoint() -> EndPoint?
}


