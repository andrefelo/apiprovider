//
//  EndPoint.swift
//
//  Copyright (c) Andres F. Lozano
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

import Alamofire
import Foundation


public struct EndPoint {
  
  
  // ---------------------------------------------------------------------
  // MARK: variables
  // ---------------------------------------------------------------------
  
  
  public var BASE_URL:String = ""
  fileprivate var encoding: Encoding
  fileprivate var headers: HTTPHeaders?
  fileprivate var method: HTTPMethod
  fileprivate var path: String
  fileprivate var parameters: [String: Any]?
  fileprivate var queryparams: [String: Any]?
  fileprivate var baseUrl: URL?
  fileprivate var authorizationTokenType: AuthorizationTokenType
  fileprivate var noToken: Bool
  
  
  
  // ---------------------------------------------------------------------
  // MARK: Constructor
  // ---------------------------------------------------------------------
  
  
  public init(
    method: HTTPMethod = .get,
    path: String...,
    baseUrl: URL? = nil,
    parameters: [String: Any]? = nil,
    queryparams: [String: Any]? = nil,
    encoding: Encoding = .default,
    headers: HTTPHeaders? = nil,
    authorizationTokenType: AuthorizationTokenType = .jwt,
    noToken: Bool = false
  ) {
    self.method = method
    self.path = path.joined(separator: "/")
    self.parameters = parameters
    self.queryparams = queryparams
    self.encoding = encoding
    self.authorizationTokenType = authorizationTokenType
    self.noToken = noToken
    self.baseUrl = baseUrl ?? defaultURL
  }
}

extension EndPoint  {
  
  
  // ---------------------------------------------------------------------
  // MARK: Helpers func
  // ---------------------------------------------------------------------
  
  
  public func asURLRequest(timeoutRequest: Double = 30) -> DataRequest? {
    guard var url = baseUrl?.appendingPathComponent(path) else { return nil }
    
    if let queryparams = queryparams {
      url.addQueryParams(queryparams)
    }
    
    return  AF.request(
      url.absoluteString,
      method: method,
      parameters: parameters,
      encoding: encoding.getEncoding(),
      interceptor: AccessTokenAdapter(tokenType: authorizationTokenType, noToken: noToken),
      requestModifier: { $0.timeoutInterval = timeoutRequest }
    )
  }
  
  var defaultURL: URL? {
    return URL(string: ApiRequestManager.url)
  }
}


extension EndPoint {
  
  // ---------------------------------------------------------------------
  // MARK: Enums
  // ---------------------------------------------------------------------
  
  
  public enum Encoding  {
    
    case destination (type: URLEncoding.Destination)
    case arrayEncoding (type: URLEncoding.ArrayEncoding)
    case boolEncoding (type: URLEncoding.BoolEncoding)
    case `default`
    case httpBody
    
    
    // ---------------------------------------------------------------------
    // MARK: Helpers func
    // ---------------------------------------------------------------------
    
    
    func getEncoding() -> ParameterEncoding {
      switch self {
        case .destination(let type):
          return URLEncoding(destination: type)
        case .arrayEncoding(let type):
          return URLEncoding(arrayEncoding: type)
        case .boolEncoding(let type):
          return URLEncoding(boolEncoding: type)
        case .default:
          return URLEncoding()
        case .httpBody:
          return URLEncoding(destination: .httpBody)
      }
    }
  }
}




struct AccessTokenAdapter: RequestInterceptor {
  
  // ---------------------------------------------------------------------
  // MARK: Variables
  // ---------------------------------------------------------------------
  
  
  var tokenType: AuthorizationTokenType
  var noToken: Bool
  
  // ---------------------------------------------------------------------
  // MARK: Constructor
  // ---------------------------------------------------------------------
  
  
  init(tokenType:AuthorizationTokenType = .jwt, noToken: Bool) {
    self.tokenType = tokenType
    self.noToken = noToken
  }
  
  
  // ---------------------------------------------------------------------
  // MARK: Helpers func
  // ---------------------------------------------------------------------
  
  
  func adapt(
    _ urlRequest: URLRequest,
    for session: Session,
    completion: @escaping (Result<URLRequest, Error>) -> Void
  ) {
    
    var urlRequest = urlRequest
    
    guard !noToken,
      let urlString = urlRequest.url?.absoluteString,
          urlString.hasPrefix(ApiRequestManager.url),
          let token = ApiRequestManager.handleGetToken?()
    else {
      completion(.success(urlRequest))
      return
    }
    
    switch tokenType {
      case .jwt:
        urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
      case .custom(let value):
        urlRequest.setValue("\(value) \(token)", forHTTPHeaderField: "Authorization")
    }
    completion(.success(urlRequest))
  }
}


public enum AuthorizationTokenType {
  case jwt
  case custom(value: String)
}


extension URL {
  
  // ---------------------------------------------------------------------
  // MARK: Helpers func
  // ---------------------------------------------------------------------
  
  
  mutating func addQueryParams( _ params: [String: Any]) {
    var components = URLComponents(string: self.absoluteString)
    components?.queryItems = params.map { element in URLQueryItem(name: element.key, value: element.value as? String) }
    if let auxURL = components?.url {
      self = auxURL
    }
  }
}
