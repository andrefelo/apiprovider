import XCTest
@testable import AFLApiRequestManager


final class ApiProviderTests: XCTestCase {
  
  
  private static let sessionToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjUzMzUwMDI0LCJleHAiOjE2ODQ0NTQwMjR9.OjxdRHsuy6Zn8Zda_gGlSsksX3iAx4iDFObnAf6mPGI"
  
  
  override func setUp() {
    super.setUp()
    setupsut()
  }
  
  
  private func setupsut() {
    ApiRequestManager.isDebug = true
    ApiRequestManager.url = "http://localhost:3001"
  }
  
  
  func setTokenMager(token: String? = sessionToken) {
    ApiRequestManager.handleGetToken = {
      return token
    }
  }
  
  func test_request_response_not_codable() {
    let expt = XCTestExpectation()
    let sut = ApiRequestManager()
    setTokenMager()
    
    sut.executeDecodable(route: UserEndpoint.getJson) { (result: Result<[String]?, CustomError>) in
      switch result {
        case .success(let result):
          XCTAssertNotNil(result)
          expt.fulfill()
          break
        case .failure(let error):
          XCTFail("Expected to be a success but got a failure with: \(error.localizedDescription)\n\n")
          expt.fulfill()
          break
      }
    }
    wait(for: [expt], timeout: 15)
  }
  
  
  func test_request_with_token() {
    let expt = XCTestExpectation()
    let sut = ApiRequestManager()
    setTokenMager()
    sut.executeDecodable( route: UserEndpoint.getWithToken) { [weak self] (resp: Result<[User], CustomError>) in
      self?.handleRequestResult(expt: expt, result: resp)
    }
    wait(for: [expt], timeout: 15)
  }
  
  
  func test_request_custom_path() {
    let expt = XCTestExpectation()
    let sut = ApiRequestManager()
    setTokenMager()
    sut.executeDecodable( route: UserEndpoint.getCustomPath) { (resp: Result<[User], CustomError>) in
      XCTAssertNotNil(sut.request?.url?.absoluteString.contains("http://localhost2:3001"))
      expt.fulfill()
    }
    wait(for: [expt], timeout: 15)
  }
  
  
  func test_request_no_token_endpoint() {
    let expt = XCTestExpectation()
    let sut = ApiRequestManager()
    setTokenMager()
    sut.executeDecodable( route: UserEndpoint.getNoToken(enable: true)) { (result: Result<[User], CustomError>) in
      XCTAssertNil(sut.request?.allHTTPHeaderFields?.first(where: { $0.value.contains(Self.sessionToken)}))
      expt.fulfill()
    }
    wait(for: [expt], timeout: 15)
  }
  
  
  func test_request_no_set_token() {
    let expt = XCTestExpectation()
    let sut = ApiRequestManager()
    setTokenMager(token: nil)
    sut.executeDecodable( route: UserEndpoint.getNoToken(enable: false)) {(result: Result<[User], CustomError>) in
      XCTAssertNil(sut.request?.allHTTPHeaderFields?.first(where: { $0.value.contains(Self.sessionToken)}))
      expt.fulfill()
    }
    wait(for: [expt], timeout: 15)
  }
  
  
  func test_request_timeout() {
    let expt = XCTestExpectation()
    let sut = ApiRequestManager()
    sut.executeDecodable( route: UserEndpoint.getWithToken, timeoutRequest: 0.001) { (resp: Result<[User], CustomError>) in
      guard let _ = try? resp.get() else {
        XCTAssertTrue(true)
        expt.fulfill()
        return
      }
      XCTFail("Expected to be a success but got a failure\n\n")
      expt.fulfill()
    }
    wait(for: [expt], timeout: 15)
  }
  
  
  private func handleRequestResult<T:Codable, E: HError>(
    expt: XCTestExpectation,
    result: Result<T, E>
  ) {
    
    switch result {
      case .success:
        XCTAssertTrue(true)
        expt.fulfill()
      case .failure(let error):
        XCTFail("Expected to be a success but got a failure with: \(error.localizedDescription)\n\n")
        expt.fulfill()
    }
  }
  
  
  enum UserEndpoint: BaseRoute {
    
    case getWithToken
    case getNoToken(enable: Bool)
    case getCustomPath
    case getJson
    
    func getEndPoint() -> EndPoint? {
      switch self {
        case .getCustomPath:
          return .init(path: "user", "all", baseUrl: .init(string: "http://localhost2:3001") )
        case .getWithToken:
          return .init(path: "user/all")
        case .getNoToken(let enable):
          return .init(path: "user/all", noToken: enable)
        case .getJson:
          return .init(path: "user", "all", "json")
          
      }
    }
  }
  
  
  
  struct User: Codable {
    let id: Int
  }
  
  
  class CustomError: DefaultError {
    
    override var errorDescription: String? {
      return "Code: \(status) - \(error ?? "empty")"
    }
  }
}
